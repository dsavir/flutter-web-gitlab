# medium_web_gitlab

Flutter project illustrating how to host a Flutter web app in Gitlab Pages.

## Getting Started

This is (mostly) the default flutter application. The interesting file is [.gitlab-ci.yml](https://gitlab.com/dsavir/flutter-web-gitlab/-/blob/main/.gitlab-ci.yml?ref_type=heads). Check out the live site [here](https://flutter-web-gitlab-dsavir-fed7746166d8b866a5a5df5f557b9a9340ee7.gitlab.io/). Read [here](https://dsavir-h.medium.com/f476ec600a4a) for a step by step tutorial.

For questions and comments, you can comment in the [tutorial](https://dsavir-h.medium.com/f476ec600a4a), open an [issue](https://gitlab.com/dsavir/flutter-web-gitlab/-/issues), or [contact me](https://danielle-honig.com/contact/).
